# Docker

### What is Docker?

Docker is an open source containerization platform. Developers use docker to put their applications into containers and ship it anywhere.

### what is a container?

Docker containers are running instances of Docker Images. The container is the package that holds all the code for the image which is to be created. A container has it's own environment that runs a system. 

### What is the difference between Docker and VM?

</u>Virtual Machines</u>

- Have Hypervisors
- Each VM runs its own Operation System (two layers, OS Kernel and Hardware)
- Can only run a couple of VMs on an average laptop
- Can run on any OS host as it has it's own kernel

<u> Docker </u>

- Can run on a virtual machine
- Doesn't need a Hypervisor
- Can run multiple containers on a single Operating System
- Size are much smaller 
- Start and run faster
- Not compatible with all OS hosts (Kernels), you will need a Docker Toolbox
- Portable

### What are the similarities between Docker and a VM?

- Both can run on a physical server
- Both improve IT efficiency

### Main sections of Docker?

- Docker Swarm
- Docker Compose
- Docker Images
- Docker Daemon
- Docker Engine




### Main Commands for Docker


```bash

# To check if you have any containers running
$ docker ps


# To see if you have any containers, including non running
$ docker ps -a

# 
```

### Task

- go get an image of httpd

```bash
$ docker pull httpd
```
- How can you SSH into your docker container

```bash 
$ docker exec -it <containerID>
```

- Have a look around

```bash
>root@17562846b68c:/usr/local/apache2# ls
>> bin  build  cgi-bin  conf  error  htdocs  icons  include  logs	modules
```

- look at option '-dit' what does that do?

```bash
$ docker run -dit --name my-apache-app -p 8080:80 -v "$PWD":/usr/local/apache2/htdocs/ httpd:2.4  

# Then in your browser navigate to 0.0.0.0:8080
```
